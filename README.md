# Anvil

Anvil is a WordPress wrapper that aims to make development easier.

You can find out more [in the wiki](https://bitbucket.org/lewisdorigo/anvil/wiki/Home).