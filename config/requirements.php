<?php

if(version_compare(PHP_VERSION, '7.2.0') < 0) {
    trigger_error('Your PHP version must be at least 7.2.0. You have '.PHP_VERSION.'.', E_USER_ERROR);
}