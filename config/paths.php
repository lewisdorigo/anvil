<?php

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

define('CONFIG_DIR', __DIR__);
define('ROOTDIR', dirname(__DIR__));
define('WEBROOT', ROOTDIR.DS.'www');

define('WP_ROOTDIR', ROOTDIR);
define('WP_WEBROOT', WEBROOT);

define('CONTENT_DIR', DS.'app');
define('WP_CONTENT_DIR', WP_WEBROOT.CONTENT_DIR);

define('ANVIL_SOURCE', ROOTDIR.DS.'src');
define('ANVIL_THEMES', ANVIL_SOURCE.DS.'themes');


if(!defined('ABSPATH')) {
    define('ABSPATH', WP_WEBROOT.DS);
}