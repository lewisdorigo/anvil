<?php
/** DEVELOPMENT **/



/**
 * Display all errors on the site.
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



/**
 * Log errors into the Uploads directory.
 */
ini_set('log_errors', 1);
ini_set('error_log', WP_CONTENT_DIR.DS.'uploads'.DS.'debug.log');


/**
 * Enable WP Debugging.
 */
define('WP_DEBUG', true);
define('WP_DEBUG_DISPLAY', true );
define('WP_DISABLE_FATAL_ERROR_HANDLER', true );
define('SAVEQUERIES', true);
define('SCRIPT_DEBUG', true);


header('Cache-Control: no-cache, no-store');