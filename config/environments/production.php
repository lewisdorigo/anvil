<?php
/** PRODUDUCTION **/



/**
 * Turn off display of all errors.
 */
ini_set('display_errors', 0);
define('WP_DEBUG_DISPLAY', false);
define('SCRIPT_DEBUG', false);



/**
 * Disable all file modifications, including updates and update notifications.
 */
define('DISALLOW_FILE_MODS', true);



/**
 * Enable caching, for W3 Total Cache.
 */
define('WP_CACHE', true);
