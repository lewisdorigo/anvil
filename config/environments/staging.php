<?php
/** STAGING **/



/**
 * Turn off display of all errors.
 */
ini_set('display_errors', 0);
define('WP_DEBUG_DISPLAY', false);
define('SCRIPT_DEBUG', false);



/**
 * Log errors into the Uploads directory.
 */
ini_set('log_errors', 1);
ini_set('error_log', WP_CONTENT_DIR.DS.'uploads'.DS.'debug.log');



/**
 * Disable all file modifications, including updates and update notifications.
 */
define('DISALLOW_FILE_MODS', true);
