<?php namespace Anvil\Config;

use Env;
use Dotenv\Dotenv;

Env::init();

if(file_exists(WP_ROOTDIR.DS.'.env')) {
    $dotEnv = new DotEnv(WP_ROOTDIR);
    $dotEnv->load();
    $dotEnv->required([
        'DB_NAME',
        'DB_USER',
        'DB_PASSWORD',
        'BASE_URL',
    ]);
} else {
    trigger_error('No environment file was found in at '.WP_ROOTDIR.DS.'.env', E_USER_ERROR);
}



/**
 * URLs
 */
define('WP_HOME', env('BASE_URL'));
define('WP_SITEURL', env('BASE_URL'));

define('WP_CONTENT_URL', WP_HOME.CONTENT_DIR);


define('ENVIRONMENT', env('ENVIRONMENT') ?: 'development');
define('WP_ENV', ENVIRONMENT);



/**
 * DB settings
 */
define('DB_NAME', env('DB_NAME'));
define('DB_USER', env('DB_USER'));
define('DB_PASSWORD', env('DB_PASSWORD'));
define('DB_HOST', env('DB_HOST') ?: 'localhost');
define('DB_CHARSET', env('DB_CHARSET') ?: 'utf8mb4');
define('DB_COLLATE', env('DB_CHARSET') ?: '');
$table_prefix = env('DB_PREFIX') ?: 'wp_';



/**
 * Authentication Unique Keys and Salts
 */
define('AUTH_KEY', env('AUTH_KEY'));
define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
define('NONCE_KEY', env('NONCE_KEY'));
define('AUTH_SALT', env('AUTH_SALT'));
define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
define('NONCE_SALT', env('NONCE_SALT'));





define('WP_DEFAULT_THEME', env('WP_DEFAULT_THEME') ?: 'anvil');