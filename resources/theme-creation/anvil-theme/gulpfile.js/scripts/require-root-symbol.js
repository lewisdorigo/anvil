const tools = require('browserify-transform-tools')
let symbol, symbolLength, baseDir, base, init = false

module.exports = tools.makeRequireTransform("requireTransform", function(args, opts, cb) {
  const importPath = args[0]

  if(!init) {
    baseDir = opts.config.baseDir || false
    symbol = opts.config.symbol || '~/'

    base = process.cwd() + (baseDir ? '/' + baseDir : '') + '/'
    symbolLength = symbol.length
    init = true
  }

  if(importPath.substr(0, symbolLength) === symbol) {
    const localFile = opts.file.replace(base, '')
    let segments = localFile.split('/').length - 1
    let dotDotSlashes = segments ? '' : './'
    
    while(segments--) dotDotSlashes += '../'
    
    return cb(null, "require('"+dotDotSlashes + importPath.substr(symbolLength)+"')")
  }
  
  return cb()
})