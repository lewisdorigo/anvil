const gulp = require('gulp');

gulp.task("build", gulp.series("constants", gulp.parallel("compile:css", "compile:css:components", "compile:js", "images"), "modernizr"));
