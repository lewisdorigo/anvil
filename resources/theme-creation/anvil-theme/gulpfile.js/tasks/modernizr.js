const gulp      = require('gulp'),
      plumber   = require('gulp-plumber'),
      uglify    = require('gulp-uglify'),
      modernizr = require('gulp-modernizr');

const config = require('../config');

/**
 * MODERNIZR
 *
 * Generates a modernizr.js file with the specified tests.
 */
gulp.task('modernizr', (done) => {
	return gulp.src([`${config.dest_root}/js/app.min.js`, `${config.dest_root}/css/app.min.css`])
	  .pipe(plumber())
		.pipe(modernizr({
			cache : true,
			tests: [],
			excludeTests: ["hidden", "video"],
			options: [
				"setClasses"
			]
		}))
		.pipe(uglify())
	  .pipe(plumber.stop())
		.pipe(gulp.dest(`${config.dest_root}/js`));
});
