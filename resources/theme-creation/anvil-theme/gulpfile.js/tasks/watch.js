const gulp        = require('gulp'),
      browserSync = require('browser-sync'),
      config      = require('../config');

gulp.task('watch', (done) => {
  gulp.watch([
    `${config.templates_root}/**/*.js`,
    `js/**/*.js`,
  ], gulp.series('compile:js','modernizr'));

  gulp.watch([
    `${config.templates_root}/**/*.{sass,scss}`,
    `scss/**/*.{sass,scss}`,
  ], gulp.parallel('compile:css'));

  gulp.watch([
    `${config.templates_root}/**/*.{php,html,twig}`,
  ]).on('change', browserSync.reload);

  gulp.watch(`images/**/*.{svg,png,jpg,gif}`, gulp.series('images'));

  gulp.watch(`images/sketch/icons.sketch`, gulp.series('sketch:icon','compile:css'));
  gulp.watch(`images/sketch/svg.sketch`, gulp.series('sketch:svg'));
  gulp.watch(`images/sketch/png.sketch`, gulp.series('sketch:png'));

  gulp.watch(`images/sketch/touch-icon.sketch`, gulp.series('sketch:touchicon'));
  gulp.watch(`images/sketch/favicon.sketch`, gulp.series('sketch:favicon'));

  gulp.watch(`constants.json`, gulp.series('constants',gulp.parallel('compile:css','compile:css:components', 'compile:js')));

  return done();
});
