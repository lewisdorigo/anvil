const gulp         = require('gulp')
const glob         = require('glob')
const sass         = require('gulp-sass')
const sassGlob     = require('gulp-sass-glob')
const autoPrefixer = require('gulp-autoprefixer')
const args         = require('yargs').argv
const wrap         = require('gulp-wrap')
const base64       = require('gulp-base64')
const sourceMaps   = require('gulp-sourcemaps')
const rename       = require('gulp-rename')
const gulpIf       = require('gulp-if')
const browserSync  = require('browser-sync')
const sprout       = require('sprout-css').includePaths
const bourbon      = require('bourbon').includePaths
const plumber      = require('gulp-plumber');

const config = require('../config');

gulp.task('compile:css:components', () => {
  return gulp.src(`${config.templates_root}/index.scss`)
    .pipe(sourceMaps.init())
    .pipe(wrap(`@import '../scss/**/*.scss';
                @import '🌱';
                @import '../gulpfile.js/cache/**/*.scss';
                @import 'css/**/*.scss';
                @import 'components/**/*.scss';`))
    .pipe(sassGlob())
    .pipe(sass({
      sourcemap: true,
      trace: true,
      outputStyle: 'compressed',
      includePaths: [].concat(sprout, config.srcRoot, config.baseRoot)
    }).on('error', sass.logError))

    .pipe(wrap(`#editor .acf-block-preview { <%= contents %> }`))
    .pipe(sass({
      sourcemap: false,
      trace: true,
      outputStyle: 'expanded',
    }).on('error', sass.logError))

    .pipe(autoPrefixer())
    .pipe(base64({
      baseDir: `${config.dest_root}/css`,
      extensions: ['svg', 'png', 'gif'],
      exclude: ['fonts'],
      maxImageSize: 4 * 1024
    }))
    .pipe(rename('editor.min.css'))
    .pipe(sourceMaps.write('maps', {
      includeContent: false,
      sourceRoot: 'sass'
    }))
    .pipe(gulp.dest(`${config.dest_root}/css`))
    .pipe(browserSync.stream({match: '**/*.css'}))
});
