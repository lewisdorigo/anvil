const gulp        = require('gulp'),
      babelify    = require('babelify'),
      browserify  = require('browserify'),
      plumber     = require('gulp-plumber'),
      uglify      = require('gulp-uglify'),
      sourceMaps  = require('gulp-sourcemaps'),
      buffer      = require('vinyl-buffer'),
      source      = require('vinyl-source-stream'),
      browserSync = require('browser-sync'),
      rename      = require('gulp-rename');

const config = require('../config');


gulp.task('compile:js', (done) => {

  var bundleStream = browserify({
    debug: true,
    transform: [
      'babelify',
      'browserify-shim',
    ]
  }).add(`${config.templates_root}/index.js`).bundle();

  return bundleStream
    .pipe(plumber())
    .pipe(source('app.min.js'))
    .pipe(buffer())
    .pipe(sourceMaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(sourceMaps.write('.'))
    .pipe(gulp.dest(`${config.dest_root}/js`))
    .pipe(browserSync.reload({stream: true}));
});
