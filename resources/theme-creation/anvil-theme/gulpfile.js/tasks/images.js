const gulp        = require('gulp'),
      imagemin    = require('gulp-imagemin'),
      changed     = require('gulp-changed'),
      browserSync = require('browser-sync'),

      svgOptimize = require('../config/svgOptimize.json');

const config = require('../config');

gulp.task('images', () => {
  return gulp.src([
      'images/**/*.{svg,png,jpg,gif}',
    ])
    .pipe(imagemin([
      imagemin.mozjpeg({
        quality: 80, 
        progressive: true,
      }),
      imagemin.optipng({
        optimizationLevel: 5,
      }),
      imagemin.svgo({
        plugins: svgOptimize
      }),
    ]))
    .pipe(changed(config.dest_root))
    .pipe(gulp.dest(`${config.dest_root}/img`))
    .pipe(browserSync.reload({stream: true}))
});
