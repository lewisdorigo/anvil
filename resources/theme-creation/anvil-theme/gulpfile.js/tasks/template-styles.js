const gulp   = require('gulp'),
      gutil  = require('gulp-util'),
      config = require('../config');

/**
 * `create_file()`
 *
 * Takes a filename and string, and writes a file with that name, with the
 * string used as the file’s contents.
 */
function create_file(filename, string) {
    var src = require('stream').Readable({ objectMode: true });

    src._read = function () {
        this.push(new gutil.File({
            cwd: "",
            base: "",
            path: filename,
            contents: new Buffer(string)
        }));

        this.push(null);
    }

    return src;
}

gulp.task('template:styles', () => {
  var pkg = require('../../package.json');

  var style = '';

  var name = pkg.name || theme,
      homepage = pkg.homepage || null,
      description = pkg.description || '',
      version = pkg.version || '1.0.0',
      author = pkg.author || null,
      tags = pkg.keywords || [];

  style += "/*!\n";
  style += name ? `Theme Name: ${name}\n` : '';
  style += homepage ? `Theme URI: ${homepage}\n` : '';
  style += description ? `Description: ${description}\n` : '';
  style += version ? `Version: ${version}\n` : '';

  if(author && typeof author === 'object') {

    style += typeof author.name !== 'undefined' ? `Author: ${author.name}\n` : '';
    style += typeof author.url  !== 'undefined' ? `Author URI: ${author.url}\n` : '';

  } else if(author && typeof author === 'string') {

      style += `Author: ${author.name}\n`;

  }

  style += tags.length > 0 ? `Tags: ${tags.join(', ')}\n` : '';
  style += "Template: anvil-core\n";

  style += "*/";

  return create_file("style.css", style)
    .pipe(gulp.dest(config.wordpress_path));
});
