const gulp = require('gulp'),
      del  = require('del');

const config = require('../config');

gulp.task('clean-del', () => {
  return del([

    // cache
    `${config.gulp_cache}/**`,
    `!${config.gulp_cache}`,
    `!${config.gulp_cache}/.gitkeep`,

    // theme
    `${config.dest_root}/**`,
    `!${config.dest_root}`,
    `!${config.dest_root}/.gitkeep`,

  ])
});
