const path = require('path'),
      dotenv = require('dotenv');

const theme_base      = process.env.INIT_CWD,
      theme_name      = path.basename(theme_base),
      templates_root  = `${theme_base}/views`,
      project_root    = theme_base.replace(new RegExp(`\/src\/themes\/${theme_name}`,'i'), ''),
      web_root        = `${project_root}/www`,
      wordpress_path  = `${web_root}/app/themes/${theme_name}`,
      dest_root       = `${wordpress_path}/assets`,
      gulp_cache      = `${theme_base}/gulpfile.js/cache`;

dotenv.config({
    path: `${project_root}/.env`
});

module.exports = {
    theme_base,
    theme_name,
    templates_root,
    project_root,
    wordpress_path,
    dest_root,
    gulp_cache,
    domain: process.env.BASE_URL || null
}