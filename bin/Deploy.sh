#!/bin/bash
#
# Bash script to deploy site.
#

set -e
cd "$(dirname "${BASH_SOURCE[0]}")"
source ./Deployment/yaml.sh


ENVIRONMENT=$1
ACTION=$2

HOST=""
USER=""
DIRNAME=""
BRANCH="master"
COMPOSER="true"

if [[ -z "$ENVIRONMENT" ]]; then
    echo "No environment provided."
    exit 1
fi

if [[ -z "$ACTION" ]]; then
    ACTION='push'
fi

create_variables ../config/deployment.yml

eval HOST='$'"$ENVIRONMENT"_host
eval USER='$'"$ENVIRONMENT"_user
eval DIRNAME='$'"$ENVIRONMENT"_path
eval BRANCH='$'"$ENVIRONMENT"_branch
eval COMPOSER='$'"$ENVIRONMENT"_composer

if [[ -z "$HOST" ]] && [[ -n "$default_host" ]]; then
    HOST=$default_host
elif [[ -z "$HOST" ]] && [[ -z "$default_host" ]]; then
    echo "No host provided."
    exit 1
fi


if [[ -z "$USER" ]] && [[ -n "$default_user" ]]; then
    USER=$default_user
elif [[ -z "$USER" ]] && [[ -z "$default_user" ]]; then
    echo "No user provided."
    exit 1
fi


if [[ -z "$DIRNAME" ]] && [[ -n "$default_path" ]]; then
    DIRNAME=$default_path
elif [[ -z "$DIRNAME" ]] && [[ -z "$default_path" ]]; then
    echo "No path provided."
    exit 1
fi


if [[ -z "$BRANCH" ]] && [[ -n "$default_branch" ]]; then
    BRANCH=$default_branch
elif [[ -z "$BRANCH" ]] && [[ -z "$default_branch" ]]; then
    BRANCH="master"
fi


if [[ -z "$COMPOSER" ]] && [[ -n "$default_composer" ]]; then
    COMPOSER=$default_composer
elif [[ -z "$COMPOSER" ]] && [[ -z "$default_branch" ]]; then
    COMPOSER="true"
fi

echo "Running action \`$ACTION\` on "$ENVIRONMENT"…"

ssh "$USER"@"$HOST" bash -s -- < ./Deployment/"$ACTION".sh --branch "$BRANCH" --path "$DIRNAME" --composer "$COMPOSER"
