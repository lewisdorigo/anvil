<?php namespace Anvil\Installer;

use Composer\Composer;
use Composer\Factory;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Installer\InstallerEvent;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class Environment {
    private static $io;
    private static $vendor;
    private static $project;
    
    private static $environment = [
        'ENVIRONMENT' => 'development',
        'BASE_URL' => null
    ];
    
    private static $db = [
        'DB_NAME' => null,
        'DB_USER' => null,
        'DB_PASSWORD' => null,
        'DB_HOST' => '127.0.0.1',
        'DB_PREFIX' => 'wp_'
    ];
    
    private static $salts = [
        'AUTH_KEY' => null,
        'SECURE_AUTH_KEY' => null,
        'LOGGED_IN_KEY' => null,
        'NONCE_KEY' => null,
        'AUTH_SALT' => null,
        'SECURE_AUTH_SALT' => null,
        'LOGGED_IN_SALT' => null,
        'NONCE_SALT' => null
    ];
    
    public static function generate($event) {   
        self::$io = $event->getIO();
        self::$vendor = $event->getComposer()->getConfig()->get('vendor-dir');
        self::$project = dirname(self::$vendor);
        
        self::createSalts();
        
        self::getEnvironment();
        self::getDBDetails();
        
        $env = array_merge(self::$environment, self::$db, self::$salts);
        
        self::writeFile($env);
    }
    
    private static function getEnvironment() {
        self::$io->write(' ');
        
        self::$environment["ENVIRONMENT"]  = self::$io->select("Environment (development)", ["development" => "development", "staging" => "staging", "live" => "live"], "development");
        self::$environment["BASE_URL"] = self::$io->ask("Site URL (http://example.test): ", 'http://example.test');
        
        if(!self::confirmDetails(self::$environment, 'environment')) { self::getEnvironment(); }
    }
    
    private static function confirmDetails(array $data, string $title = "the data") {
        self::$io->write(' ');
        
        return self::$io->askConfirmation('Are you happy with the '.$title.'? (yes): ');
    }
    
    private static function getDBDetails() {
        self::$io->write(' ');
        self::$io->write("Database");

        self::$db["DB_NAME"]     = self::$io->ask("Database Name: ");
        self::$db["DB_USER"]     = self::$io->ask("Database User: ");
        self::$db["DB_PASSWORD"] = self::$io->askAndHideAnswer("Database Password: ");
        self::$db["DB_HOST"]     = self::$io->ask("Database Host (127.0.0.1): ", '127.0.0.1');
        self::$db["DB_PREFIX"]   = self::$io->ask("Databse Prefix (wp_): ", "wp_");
        
        if(!self::confirmDetails(self::$db, 'database details')) { self::getDBDetails(); }
    }
    
    private static function createSalts() {
        self::$salts = array_map(function() {
            return self::getSalt();
        }, self::$salts);
    }
    
    private static function getSalt(int $len = 64) {
         $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\;:?.>,<!@$%^&*()-_=+|';
    
         $randString = "";
         for ($i = 0; $i < $len; $i++) {
             $randString .= $charset[mt_rand(0, strlen($charset) - 1)];
         }
    
         return $randString;
    }
    
    public static function formatValue($value) {
            
        if(is_string($value)) {
            
            return "'{$value}'";
            
        } elseif(is_bool($value)) {
            
            return $value ? 'true' : 'false'; 
            
        } elseif(is_array($value)) {
            
            return current($value);
            
        }
        
        return $value;
    }
    
    private static function writeFile(array $data) {
        
        if(!file_exists(self::$project.'/.env.example')) {
            self::$io->writeError('ERROR: There is no `.env.example` file');
            return false;
        }
        
        $template = file_get_contents(self::$project.'/.env.example');
        $lines = explode(PHP_EOL,$template);
        
        foreach($data as $key => $value) {
            $key = strtoupper($key);
            $value = self::formatValue($value);
            
            $lines = preg_replace('/^('.$key.'=).*/', "{$key}={$value}", $lines);
        }
        
        file_put_contents(self::$project.'/.env', implode(PHP_EOL, $lines));
        
        self::$io->write(' ');
        self::$io->write('.env file written to '.self::$project.'/.env');
        self::$io->write('You also can run this again by running `composer generate-env` at any time.');
        self::$io->write(' ');
    }
}