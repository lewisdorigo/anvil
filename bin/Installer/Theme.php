<?php namespace Anvil\Installer;

use Composer\Composer;
use Composer\Factory;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Installer\InstallerEvent;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class Theme {
    private static $io;
    private static $vendor;
    private static $project;

    private static $templatesDir = 'resources/theme-creation';
    private static $themeName = 'anvil';

    private static $forbiddenNames = [
        'anvil-core'
    ];

    private static $details = [
        'name' => 'Anvil',
        'version' => '1.0.0',
        'description' => '',
        'author' => [
            'name' => '',
            'email' => '',
            'url' => '',
        ],
        'homepage' => ''
    ];

    public static function create($event) {
        self::$io = $event->getIO();
        self::$vendor = $event->getComposer()->getConfig()->get('vendor-dir');
        self::$project = dirname(self::$vendor);

        self::getThemeDetails();
        self::copyAnvilTemplate();
        self::copyWordPressTemplate();
    }

    private static function getThemeDetails() {
        self::$io->write('');

        self::$details['name'] = self::$io->ask('Theme Name (Anvil): ', 'Anvil');
        self::$details['homepage'] = self::$io->ask('Theme Website: ');
        self::$details['description'] = self::$io->ask('Description: ');

        self::$details['author']['name'] = self::$io->ask('Author Name: ');
        self::$details['author']['email'] = self::$io->ask('Author Email: ');
        self::$details['author']['url'] = self::$io->ask('Author Website: ');

        self::$themeName = self::$details['name'];
        self::$themeName = self::sanitizeString(self::$themeName);

        if(in_array(self::$themeName, self::$forbiddenNames)) {
            self::$io->writeError('ERROR: Your theme cannot be named `'.self::$themeName.'`.');
            self::$io->write('You can try again by running `composer create-theme`.');

            die();
        }
    }

    private static function copyAnvilTemplate() {

        $themeDir = self::copyTemplate('anvil-theme', 'src/themes/'.self::$themeName, 'Anvil template');
        self::updatePackageJson($themeDir);
        self::npmInstall($themeDir);

    }

    private static function npmInstall($path) {

        self::$io->write('');
        if(self::$io->askConfirmation("Run `npm install`? (yes) ")) {
            shell_exec("cd {$path} && npm install --ansi");
            self::$io->write('Done');
        }

    }

    private static function copyWordPressTemplate() {

        $themeDir = self::copyTemplate('wordpress-theme', 'www/app/themes/'.self::$themeName, 'WordPress template');
        self::updateWordPressStyles($themeDir);

    }

    private static function copyTemplate(string $from, string $to, string $type = 'template') {

        $fromDir = self::$project.'/'.self::$templatesDir.'/'.ltrim($from,'/');
        $toDir = self::$project.'/'.ltrim($to,'/');
        self::$io->write('');

        if(!file_exists($fromDir) || !is_dir($fromDir)) {

            self::$io->writeError('ERROR: The '.$type.' `'.$from.'` does not exist.');
            die;

        } else if(file_exists($toDir) && is_dir($toDir)) {

            self::$io->write('The '.$type.' `'.$to.'` already exists.');
            return $toDir;

        } else {

            self::$io->write('Copying '.$type.' to `'.$toDir.'`.');
            shell_exec("cp -r {$fromDir} {$toDir}");
            self::$io->write('Done.');

            return $toDir;

        }
    }

    private static function updatePackageJson($path) {
        $packageJson = $path.'/package.json';

        self::$io->write('');
        self::$io->write('Updating package.json…');

        if(!file_exists($packageJson)) {
            self::$io->writeError('ERROR: There is no `package.json` file');
            return false;
        }

        $json = file_get_contents($packageJson);
        $json = json_decode($json, true);

        $json = array_merge($json, self::$details);

        file_put_contents($packageJson, json_encode($json, JSON_PRETTY_PRINT));

        self::$io->write('Done.');
    }

    private static function updateWordPressStyles($path) {

        self::$io->write('');
        self::$io->write('Updating WordPress theme metadata…');

        $styles = $path.'/style.css';

        $lines = [
            '/*!',
            'Theme Name: '.self::$details['name'],
            'Theme URI: '.self::$details['homepage'],
            'Description: '.self::$details['description'],
            'Version: '.self::$details['version'],
            'Author: '.self::$details['author']['name'],
            'Author URI: '.self::$details['author']['url'],
            'Template: anvil-core',
            '*/'
        ];

        file_put_contents($styles, implode(PHP_EOL,$lines));

        self::$io->write('Done.');
    }

    private static function sanitizeString(string $string) {
        $string = strip_tags( $string );
        // Preserve escaped octets.
        $string = preg_replace( '|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $string );
        // Remove percent signs that are not part of an octet.
        $string = str_replace( '%', '', $string );
        // Restore octets.
        $string = preg_replace( '|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $string );

        $string = strtolower( $string );

        // Convert nbsp, ndash and mdash to hyphens
        $string = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $string );
        // Convert nbsp, ndash and mdash HTML entities to hyphens
        $string = str_replace( array( '&nbsp;', '&#160;', '&ndash;', '&#8211;', '&mdash;', '&#8212;' ), '-', $string );
        // Convert forward slash to hyphen
        $string = str_replace( '/', '-', $string );

        // Strip these characters entirely
        $string = str_replace(
                array(
                        // soft hyphens
                        '%c2%ad',
                        // iexcl and iquest
                        '%c2%a1',
                        '%c2%bf',
                        // angle quotes
                        '%c2%ab',
                        '%c2%bb',
                        '%e2%80%b9',
                        '%e2%80%ba',
                        // curly quotes
                        '%e2%80%98',
                        '%e2%80%99',
                        '%e2%80%9c',
                        '%e2%80%9d',
                        '%e2%80%9a',
                        '%e2%80%9b',
                        '%e2%80%9e',
                        '%e2%80%9f',
                        // copy, reg, deg, hellip and trade
                        '%c2%a9',
                        '%c2%ae',
                        '%c2%b0',
                        '%e2%80%a6',
                        '%e2%84%a2',
                        // acute accents
                        '%c2%b4',
                        '%cb%8a',
                        '%cc%81',
                        '%cd%81',
                        // grave accent, macron, caron
                        '%cc%80',
                        '%cc%84',
                        '%cc%8c',
                ),
                '',
                $string
        );

        // Convert times to x
        $string = str_replace( '%c3%97', 'x', $string );

        $string = preg_replace( '/&.+?;/', '', $string ); // kill entities
        $string = str_replace( '.', '-', $string );

        $string = preg_replace( '/[^%a-z0-9 _-]/', '', $string );
        $string = preg_replace( '/\s+/', '-', $string );
        $string = preg_replace( '|-+|', '-', $string );
        $string = trim( $string, '-' );

        return $string;
    }
}