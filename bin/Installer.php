<?php namespace Anvil;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Installer\InstallerEvent;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

use Anvil\Installer\{Environment, Theme};

class Installer {
    private static $io;

    public static function install($event) {
        self::$io = $event->getIO();

        if(self::$io->askConfirmation("Do you want to define your .env file interactively? (yes): ")) {
            Environment::generate($event);
        }

        if(self::$io->askConfirmation("Do you want to create a new theme? (yes): ")) {
            Theme::create($event);
        }
    }
}