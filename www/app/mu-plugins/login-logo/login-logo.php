<?php
/**
 * Plugin Name:       Login Logo
 * Plugin URI:        http://dorigo.co/
 * Description:       Adds the Anvil logo to the WP Admin
 * Version:           1.0.0
 * Author:            Lewis Dorigo
 * Author URI:        http://dorigo.co/
 */


namespace Anvil\Plugins;

Class LoginLogo {
    static $instance;

    private function __construct() {
        add_action('login_head', [$this,'addStyles'], 9);

        add_filter('login_headertext', [$this, 'headerText'], 9);
        add_filter('login_headerurl', [$this, 'logoLink'], 9);
    }

    public static function getInstance() {
        if(!self::$instance) {
            self::$instance = new LoginLogo();
        }

        return self::$instance;
    }

    public function headerText() {
        return apply_filters('Anvil/LoginLogo/Text', 'Powered by Anvil, with WordPress.');
    }

    public function logoLink() {
        return apply_filters('Anvil/LoginLogo/Url', 'https://dorigo.co/');
    }

    public function addStyles() {
        $logo = apply_filters('Anvil/LoginLogo/Logo', plugins_url('anvil.svg', __FILE__));
        $width = apply_filters('Anvil/LoginLogo/Width', 110);
        $height = apply_filters('Anvil/LoginLogo/Height', 79);
        $margin = apply_filters('Anvil/LoginLogo/Margin', 48);

        echo "<style>
    	h1 a {
    	    background-image: url({$logo})!important;
    	    background-size: {$width}px {$height}px!important;
    	    width: {$width}px!important;
    	    height: {$height}px!important;
            margin-bottom: {$margin}px!important;
        }
    	</style>";
    }
}

LoginLogo::getInstance();